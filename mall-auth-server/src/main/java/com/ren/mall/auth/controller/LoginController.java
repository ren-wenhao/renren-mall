package com.ren.mall.auth.controller;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.ren.common.constant.AuthServerConstant;
import com.ren.common.exception.BizCodeEnum;
import com.ren.common.utils.R;
import com.ren.mall.auth.feign.ThirdPartFeignService;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: LoginController</p>
 * Description：登录注册模块
 * date：2020/6/25 13:02
 */
@Controller
public class LoginController {

	@Autowired
	ThirdPartFeignService thirdPartFeignService;

	@Autowired
	StringRedisTemplate redisTemplate;

	@ResponseBody
	@GetMapping("/sms/sendcode")
	public R sendCode(@RequestParam("phone") String phone){

		//TODO 1 接口防刷


		String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);

		if(!StringUtils.isEmpty(redisCode)){
			long l = Long.parseLong(redisCode.split("_")[1]);
			if(System.currentTimeMillis() - l < 60000){
				// 60s内不能在发送
				return R.error(BizCodeEnum.SMS_CODE_EXCEPTION.getCode(),BizCodeEnum.SMS_CODE_EXCEPTION.getMsg());
			}
		}


		// 2 验证码的再次检验，使用redis
		String code = UUID.randomUUID().toString().substring(0,5) + "_" + System.currentTimeMillis();

		// redis缓存验证码，防止同一个手机号60s内再次发送验证码
		redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone, code, 10, TimeUnit.MINUTES);

		thirdPartFeignService.sendCode(phone,code);
		return R.ok();
	}

	@PostMapping("regist")
	public String regist() {
		// 注册成功回到首页，回到登录页
		return "redirect:/login.html";
	}

}
