package com.ren.mall.auth.vo;

import lombok.Data;

/**
 * <p>Title: UserLoginVo</p>
 * Description：
 * date：2021/5/25 21:38
 */
@Data
public class UserLoginVo {

	private String loginacct;

	private String password;
}
