package com.ren.mall.order.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.ren.common.vo.MemberRsepVo;
import com.ren.mall.order.vo.MemberAddressVo;
import com.ren.mall.order.vo.OrderConfirmVo;
import com.ren.mall.order.vo.OrderItemVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ren.common.utils.PageUtils;
import com.ren.common.utils.Query;

import com.ren.mall.order.dao.OrderDao;
import com.ren.mall.order.entity.OrderEntity;
import com.ren.mall.order.service.OrderService;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }

    //@Override
    //public OrderConfirmVo confirmOrder()throws ExecutionException, InterruptedException {
    //    MemberRsepVo memberRsepVo = LoginUserInterceptor.threadLocal.get();
    //    OrderConfirmVo confirmVo = new OrderConfirmVo();
    //
    //    // 这一步至关重要 冲主线程获取用户数据 异步线程来共享
    //    RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
    //    CompletableFuture<Void> getAddressFuture = CompletableFuture.runAsync(() -> {
    //        // 异步线程共享 RequestContextHolder.getRequestAttributes()
    //        RequestContextHolder.setRequestAttributes(attributes);
    //        // 1.远程查询所有的收获地址列表
    //        List<MemberAddressVo> address;
    //        try {
    //            address = memberFeignService.getAddress(memberRsepVo.getId());
    //            confirmVo.setAddress(address);
    //        } catch (Exception e) {
    //            log.warn("\n远程调用会员服务失败 [会员服务可能未启动]");
    //        }
    //    }, executor);
    //
    //
    //    CompletableFuture<Void> cartFuture = CompletableFuture.runAsync(() -> {
    //        // 异步线程共享 RequestContextHolder.getRequestAttributes()
    //        RequestContextHolder.setRequestAttributes(attributes);
    //        // 2. 远程查询购物车服务
    //        // feign在远程调用之前要构造请求 调用很多拦截器
    //        List<OrderItemVo> items = cartFeignService.getCurrentUserCartItems();
    //        confirmVo.setItems(items);
    //    }, executor).thenRunAsync(()->{
    //        RequestContextHolder.setRequestAttributes(attributes);
    //        List<OrderItemVo> items = confirmVo.getItems();
    //        // 获取所有商品的id
    //        List<Long> collect = items.stream().map(item -> item.getSkuId()).collect(Collectors.toList());
    //        R hasStock = wmsFeignService.getSkuHasStock(collect);
    //        List<SkuStockVo> data = hasStock.getData(new TypeReference<List<SkuStockVo>>() {});
    //        if(data != null){
    //            // 各个商品id 与 他们库存状态的映射
    //            Map<Long, Boolean> stocks = data.stream().collect(Collectors.toMap(SkuStockVo::getSkuId, SkuStockVo::getHasStock));
    //            confirmVo.setStocks(stocks);
    //        }
    //    },executor);
    //    // 3.查询用户积分
    //    Integer integration = memberRsepVo.getIntegration();
    //    confirmVo.setIntegration(integration);
    //
    //    // 4.其他数据在类内部自动计算
    //    // TODO 5.防重令牌
    //    String token = UUID.randomUUID().toString().replace("-", "");
    //    confirmVo.setOrderToken(token);
    //    stringRedisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRsepVo.getId(), token, 10, TimeUnit.MINUTES);
    //    CompletableFuture.allOf(getAddressFuture, cartFuture).get();
    //    return confirmVo;
    //}

}
