package com.ren.mall.order.dao;

import com.ren.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:52:19
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {

}
