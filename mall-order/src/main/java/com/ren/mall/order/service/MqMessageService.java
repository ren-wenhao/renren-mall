package com.ren.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ren.common.utils.PageUtils;
import com.ren.mall.order.entity.MqMessageEntity;

import java.util.Map;

/**
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:52:18
 */
public interface MqMessageService extends IService<MqMessageEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

