package com.ren.mall.order.dao;

import com.ren.mall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:52:19
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {

}
