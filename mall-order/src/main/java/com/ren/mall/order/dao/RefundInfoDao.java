package com.ren.mall.order.dao;

import com.ren.mall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:52:18
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {

}
