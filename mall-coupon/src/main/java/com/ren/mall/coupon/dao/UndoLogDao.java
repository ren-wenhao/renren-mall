package com.ren.mall.coupon.dao;

import com.ren.mall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:16:43
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {

}
