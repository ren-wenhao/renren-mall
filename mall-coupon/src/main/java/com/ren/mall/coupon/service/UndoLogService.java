package com.ren.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ren.common.utils.PageUtils;
import com.ren.mall.coupon.entity.UndoLogEntity;

import java.util.Map;

/**
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:16:43
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

