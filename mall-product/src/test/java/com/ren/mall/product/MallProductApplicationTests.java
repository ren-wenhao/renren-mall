package com.ren.mall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ren.mall.product.entity.BrandEntity;
import com.ren.mall.product.service.BrandService;
import com.ren.mall.product.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class MallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    public void teststringRedisTemplate(){
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        // 保存
        ops.set("hello","world_"+ UUID.randomUUID().toString());
        // 查询
        String hello = ops.get("hello");
        System.out.println("之前保存的数据是："+hello);
    }

    @Test
    public void testFindPath(){
        Long[] catelogPath = categoryService.findCateLogPath(225L);
        log.info("完整路径：{}", Arrays.asList(catelogPath));
    }



    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();

        //brandEntity.setName("华为");
        //brandService.save(brandEntity);
        //System.out.println("保存成功。。。。");

        //brandEntity.setBrandId(6L);
        //brandEntity.setDescript("华为");
        //brandService.updateById(brandEntity);

        List<BrandEntity> brand = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        brand.forEach((item) -> {
            System.out.println(item);
        });
    }

    @Test
    public void testConcurrentMap () {

        ConcurrentMap<Integer, String> cmap = new ConcurrentHashMap<Integer, String>();
        cmap.put(1, "一");
        cmap.put(2, "二");
        cmap.put(3, "三");
        System.out.println();
    }
}
