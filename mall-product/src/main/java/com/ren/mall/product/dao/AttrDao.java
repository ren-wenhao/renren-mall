package com.ren.mall.product.dao;

import com.ren.mall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 商品属性
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-23 15:07:13
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {


    List<Long> selectSearchAttrIds(List<Long> attrIds);
}
