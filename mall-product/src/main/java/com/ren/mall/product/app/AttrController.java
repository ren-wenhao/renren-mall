package com.ren.mall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ren.mall.product.entity.ProductAttrValueEntity;
import com.ren.mall.product.service.ProductAttrValueService;
import com.ren.mall.product.vo.AttrRespVo;
import com.ren.mall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ren.mall.product.entity.AttrEntity;
import com.ren.mall.product.service.AttrService;
import com.ren.common.utils.PageUtils;
import com.ren.common.utils.R;


/**
 * 商品属性
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-23 16:12:10
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {

    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    //@GetMapping("/base/list/{catelogId}")
    //public R baseAttrList(@RequestParam Map<String, Object> params,
    //                      @RequestParam("catelogId") Long catelogId){
    //    PageUtils page = attrService.queryBaseAttrPage(params,catelogId);
    //    return R.ok().put("page", page);
    //}

    @GetMapping("/{attrType}/list/{catelogId}")
    public R baseAttrList(@RequestParam Map<String, Object> params ,
                          @PathVariable("catelogId") Long catelogId,
                          @PathVariable("attrType") String attrType){

        PageUtils page = attrService.queryBaseAttrPage(params, catelogId, attrType);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId){
        AttrRespVo respVo = attrService.getAttrInfo(attrId);
        R ok = R.ok();
        ok.put("data", respVo);
        return ok.put("attr", respVo);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);
        return R.ok().put("page", page);
    }

    @PostMapping("/update/{spuId}")
    public R updateSpiAttr(@PathVariable("spuId") Long spuId, @RequestBody List<ProductAttrValueEntity> entities){
        productAttrValueService.updateSpuAttr(spuId, entities);
        return R.ok();
    }

    /**
     * 查询属性规格
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrListForSpu(@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> entities = productAttrValueService.baseAttrListForSpu(spuId);
        return R.ok().put("data", entities);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrVo attrVo){
        attrService.saveAttr(attrVo);

        return R.ok();
    }

    /**
     * 更改规格参数：参数名、参数id、参数、状态的一一对应
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrVo attrVo){
        attrService.updateAttr(attrVo);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrIds){
        attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
