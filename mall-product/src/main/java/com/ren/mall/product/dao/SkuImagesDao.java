package com.ren.mall.product.dao;

import com.ren.mall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-23 15:07:13
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {

}
