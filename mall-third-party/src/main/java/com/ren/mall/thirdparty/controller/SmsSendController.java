package com.ren.mall.thirdparty.controller;

import com.ren.common.utils.R;
import com.ren.mall.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author RWH
 * @Date 2021/11/10 15:24
 * @Version 1.0
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {

    @Autowired
    SmsComponent smsComponent;

    /**
     * 提供给别的服务调用的
     * @param phone
     * @param code
     * @return
     */
    @GetMapping
    public R sendCode(@RequestParam("phone") String phone,@RequestParam("code") String code){

        return null;
    }
}
