package com.ren.mall.member.dao;

import com.ren.mall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:43:36
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {

}
