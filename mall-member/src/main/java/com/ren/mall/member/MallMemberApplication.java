package com.ren.mall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 想要远程调用别的服务
 * 1.引入open-feign依赖
 * 2.编写一个接口，告诉springcloud这个接口需要调用远程服务
 *  2.1.声明接口的每一个方法都是调用哪个远程服务的哪个请求
 * 3.开始远程调用服务
 */
@EnableFeignClients(basePackages = "com.ren.mall.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class MallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallMemberApplication.class, args);
    }

}
