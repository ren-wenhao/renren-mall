package com.ren.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ren.common.utils.PageUtils;
import com.ren.mall.member.entity.MemberEntity;
import com.ren.mall.member.exception.PhoneExistException;
import com.ren.mall.member.exception.UserNameExistException;
import com.ren.mall.member.vo.MemberLoginVo;
import com.ren.mall.member.vo.SocialUser;
import com.ren.mall.member.vo.UserRegisterVo;

import java.util.Map;

/**
 * 会员
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:43:36
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void register(UserRegisterVo userRegisterVo) throws PhoneExistException, UserNameExistException;

    void checkPhone(String phone) throws PhoneExistException;

    void checkUserName(String username) throws UserNameExistException;

    /**
     * 普通登录
     */
    MemberEntity login(MemberLoginVo vo);
    MemberEntity login(SocialUser socialUser);
}

