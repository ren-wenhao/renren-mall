package com.ren.mall.member.feign;

import com.ren.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>Title: CouponFeignService</p>
 * Description：
 * date：2020/5/30 13:35
 */
@FeignClient("mall-coupon") //声明要调用哪个模块的接口
public interface CouponFeignService {

	@RequestMapping("/coupon/coupon/member/list")
	R memberCoupons();
}
