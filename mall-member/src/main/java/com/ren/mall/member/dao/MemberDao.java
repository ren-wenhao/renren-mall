package com.ren.mall.member.dao;

import com.ren.mall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:43:36
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {

}
