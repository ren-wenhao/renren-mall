package com.ren.mall.ware.dao;

import com.ren.mall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:58:36
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {

}
