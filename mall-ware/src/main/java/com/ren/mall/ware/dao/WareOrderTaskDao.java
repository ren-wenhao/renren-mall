package com.ren.mall.ware.dao;

import com.ren.mall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 *
 * @author renwenhao
 * @email 865417202@qq.com
 * @date 2021-03-24 10:58:36
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {

}
